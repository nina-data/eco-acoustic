#!/usr/bin/env python3

import argparse
import torch
import json
import glob

from pyannote.audio.utils.signal import Binarize

class PyannotePreds():
    """
    Function that take a file and returns a csv composed of the name of the file and the speech duration.
    file_path: path to the file that needs to be processed by Pyannote
    save_folder: name of the folder where to save the csv file
    save_file: name of the csv file
    """

    def __init__(self, file_path, save_file):

        self.file_path = file_path
        self.save_file = save_file
        self.binarize = Binarize(offset=0.52, onset=0.52, log_scale=True, min_duration_off=0.6, min_duration_on=0.6)

    def get_preds(self):
        test_file = {'uri': 'file', 'audio': self.file_path}
        sad_scores = sad(test_file)

        # speech regions as a list / Can be an alternative to
        # speech.duration if we want to know WHERE is the speech
        speech = self.binarize.apply(sad_scores, dimension=1)

        return speech.for_json()

    def main(self):
        speech = self.get_preds()

        with open(self.save_file + '.json', 'w') as outfile:
            json.dump(speech, outfile)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--input",
                        help='Path to the file that will be predicted',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--output",
                        help='Path to the output folder',
                        required=True,
                        type=str,
                        )

    cli_args = parser.parse_args()

    # Device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load the PyAnnote model
    sad = torch.hub.load('pyannote/pyannote-audio', 'sad_ami', device=device, batch_size=128)

    # List the folder with files that needs predictions
    audiofiles = glob.glob(cli_args.input + "/*.WAV")

    # Make the prediction
    for audiofile in audiofiles:
        out_name = audiofile.split('/')[-1]
        PyannotePreds(audiofile, cli_args.output + out_name).main()


# docker run -it --rm -v $HOME/Data:/Data -v $PWD:/app/ eco-acoustic:latest poetry run python pyannote_predict.py --input /Data/wav_files/ --output /Data/json_eco-acoustic/pyannote/
