#!/usr/bin/env python3

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import os

from torch.utils.data import DataLoader
from sklearn.metrics import auc, roc_curve

import argparse
import xlsxwriter

from dataset_loader.audiodataset_filepath import AudioDataset
from models.VGG11 import VGG11

class PredictMetrics:

    def __init__(self, input, model_weight, output, prefix_figure):
        self.input = input
        self.model_weight = model_weight
        self.output = output
        self.prefix_figure = prefix_figure

        # related to the hardware
        self.use_cuda = torch.cuda.is_available()
        self.device = torch.device("cuda" if self.use_cuda else "cpu")
        self.batch_size = 32
        self.num_workers = 4

        # related to the model
        self.criterion = nn.BCELoss(reduction='none')
        self.model = self.initModel()

        # metrics
        self.THRESHOLD = 0.5
        self.METRICS_SIZE = 2
        self.METRICS_LABELS_NDX = 0
        self.METRICS_PREDS_NDX = 1
        
    def initDL(self):
        
        predset = AudioDataset(self.input,
                                   n_fft=1024, 
                                   hop_length=376, 
                                   n_mels=128)
        
        batch_size = self.batch_size
        if self.use_cuda:
            batch_size *= torch.cuda.device_count()
               
        predLoader = DataLoader(predset,
                                batch_size = self.batch_size, 
                                shuffle=True, 
                                num_workers=self.num_workers,
                                pin_memory=self.use_cuda)
        return predLoader
        
        
    def initModel(self):
        
        model = VGG11()
        
        if self.use_cuda:
            model.load_state_dict(torch.load(self.model_weight))
            model.to("cuda")
            
        model = model.double()
        model.eval()
        
        return model
        
    def ComputeBatchLoss(self, batch_ndx, batch_tup, metrics_mat, l_files):
        
        inputs, labels, f = batch_tup[0].to(self.device), batch_tup[1].to(self.device), batch_tup[2]
        
        outputs = self.model(inputs.double())
        outputs = outputs.squeeze(1)

        # We don't need to monitor the loss in this case
        # loss = self.criterion(outputs.double(), labels.double())
        
        start_ndx = batch_ndx * self.batch_size
        end_ndx = start_ndx + labels.size(0)
        
        metrics_mat[self.METRICS_LABELS_NDX, start_ndx:end_ndx] = labels.detach()
        metrics_mat[self.METRICS_PREDS_NDX, start_ndx:end_ndx] = outputs.detach()
        l_files[start_ndx:end_ndx] = f

        
    def plotROC(self, labels, preds):
        
        mod_fpr, mod_tpr, _ = roc_curve(labels, preds)
        roc_auc = auc(mod_fpr, mod_tpr)
        
        plt.plot(mod_fpr, mod_tpr, marker='.', label="ROC curve {} (area={:.2f})".format(self.prefix_figure, roc_auc))
        plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
        # axis labels
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        # show the legend
        plt.legend()
        # show the plot
        plt.savefig(os.path.join(self.output, self.prefix_figure + '.png'))
        

    def writeXLSX(self, labels, preds, list_file_paths):
        
        speech_indices = preds >= self.THRESHOLD
        no_speech_indices = preds < self.THRESHOLD

	    preds_value = preds
              
        preds[speech_indices] = 1
        preds[no_speech_indices] = 0
        
        # Write the summary file
        workbook = xlsxwriter.Workbook(os.path.join(self.output, self.prefix_figure +'.xlsx'))
        worksheet = workbook.add_worksheet()

        # Add bold to highlight cells
        bold = workbook.add_format({'bold': True})
        format1 = workbook.add_format({'bg_color': '#FFC7CE'})
        format2 = workbook.add_format({'bg_color': '#90ee90'})

        ######################
        # GET TP, TN, FP, FN #
        ######################
        tp = (labels == 1) & (preds == 1)
        tn = (labels == 0) & (preds == 0)
        fp = (labels == 0) & (preds == 1)
        fn = (labels == 1) & (preds == 0)

        # Adopt a "long" format
        confusion_matrix = []

        for i in range(len(tp)):
            if tp[i] == 1:
                confusion_matrix.append("tp")
            elif tn[i] == 1:
                confusion_matrix.append("tn")
            elif fp[i] == 1:
                confusion_matrix.append("fp")
            else:
                confusion_matrix.append("fn")

        ##################
        # WRITE THE XLSX #
        ##################    

        # Start from the first cell.
        # Rows and columns are zero indexed.
        row = 1
        n = len(list_file_paths) + 1

        # Write headers
        worksheet.write('A1', 'file_name', bold)
		worksheet.write('B1', 'preds_value', bold)
        worksheet.write('C1', 'pred_label', bold)
        worksheet.write('D1', 'true_label', bold)
        worksheet.write('E1', 'confusion_matrix', bold)

        # iterating through content list
        for i in range(len(list_file_paths)):

            # write operation perform
            worksheet.write(row, 0, list_file_paths[i])
            worksheet.write(row, 1, preds_value[i])
            worksheet.write(row, 2, preds[i])
            worksheet.write(row, 3, labels[i])
            worksheet.write(row, 4, confusion_matrix[i])

            # incrementing the value of row by one
            # with each iteratons.
            row += 1

        worksheet.conditional_format("E2:E{}".format(n),
                                     {"type": "formula",
                                      "criteria": '=(B2:B{}<>C2:C{})'.format(n,n),
                                      "format": format1
                                     }) 

        worksheet.conditional_format("E2:E{}".format(n),
                                     {"type": "formula",
                                      "criteria": '=(B2:B{}=C2:C{})'.format(n,n),
                                      "format": format2
                                     }) 

        workbook.close()
        
    def write_summary(self, metrics_mat, list_file_paths):
        
        preds = metrics_mat[self.METRICS_PREDS_NDX].cpu().detach().numpy()
        labels = metrics_mat[self.METRICS_LABELS_NDX].cpu().detach().numpy()
        
        self.plotROC(labels, preds)
        self.writeXLSX(labels, preds, list_file_paths)
                

    def do_pred(self):
        
        pred_dl = self.initDL()
        
        val_metrics = torch.zeros(self.METRICS_SIZE, len(pred_dl.dataset), device=self.device)
        l_files = []
        
        with torch.no_grad():
            for batch_ndx, batch_tup in enumerate(pred_dl):
                
                self.ComputeBatchLoss(batch_ndx, 
                                        batch_tup, 
                                        val_metrics,
                                         l_files)

        self.write_summary(val_metrics, l_files)
        
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()

    parser.add_argument("--input",
                        help='Path to the dataset to predict',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--model_weight",
                        help='Path to the model weights used for prediction',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--output",
                        help='Directory to save the figures',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--prefix_figure",
                        help='Prefix to identify the figure',
                        required=True,
                        type=str,
                        )

    # related to system
    cli_args = parser.parse_args()

    PredictMetrics(cli_args.input,
                   cli_args.model_weight,
                   cli_args.output,
                   cli_args.prefix_figure).do_pred()
