# Repository for: Automated speech detection in eco-acoustic data for privacy protection and human impact quantification

While many of the scripts in this repo can be used, they require general python knowledge and software implementation skills.
We built a parallel repository, [ANODE](https://gitlab.com/bencretois/anode) that contains a trained VAD model and all the
necessary scripts and instructions to facilitate the task of speech detection task for your own eco-acoustic datasets.

## Repository

This repository contains all the tools necessary to train a deep learning model for Voice Active Detection (VAD) and to anonymize your data. 

If you do not wish to train a model from scratch you can either used our own model ecoVAD or PyAnnote and you should refer directly to the section related to [making predictions](#making-predictions).

### Requirements

To train a model from scratch you will need your own soundscape dataset, a human speech dataset and a background noise dataset. 

* For the human speech dataset you might want to download [VoxCeleb](https://www.robots.ox.ac.uk/~vgg/data/voxceleb/) or [LibriSpeech](https://www.openslr.org/12/). 
* For the background noise dataset you might want to check out [ESC50](https://github.com/karolpiczak/ESC-50) or [BirdCLEF](https://www.imageclef.org/lifeclef/2017/bird).

Although not necessary, access to a GPU might be beneficial to speed up model training and predictions.

### Using docker

Even though not necessary either, it is highly encouraged to have docker installed on your system so that you can more easily reproduce the entire workflow without encountering any problems with package dependancies. To install `Docker` on your system please refer to [this page](https://docs.docker.com/get-docker/). If you use `Docker` make sure you first pull the image from the repository with:

```
docker pull docker://registry.gitlab.com/nina-data/eco-acoustic:latest
```

Then, make sure you run the line presented below as a prefix to running the script and add `/app` before the `.py` script:

```
docker run -it --rm --gpus all -v /PATH/TO/DATA/DIR:/Data/ -v $PWD:/app/ eco-acoustic:latest poetry run /app/YOUR SCRIPT.py
```

For instance, running the script `make_data.py` with docker would look like:

```
docker run -it --rm --gpus all -v /PATH/TO/DATA/DIR:/Data/ -v $PWD:/app/ eco-acoustic:latest poetry run python /app/main_scripts/make_data.py ... # include all arguments
```

### Installing the dependancies

If you do not choose to use `Docker` you will need to install all dependancies. We chose `poetry` as package manager and the dependancies are listed in the file `pyproject.toml`. If you use `anaconda` or other package managers we also provide a `requirements.txt` file.

If you do not use `Docker` also make sure to export the `PYTHONPATH`:

```
export PYTHONPATH=$PYTHONPATH:/CUSTOM/PATH/eco-acoustic/
```

## Training your own VAD model

### Building a synthetic dataset 

First, an augmented dataset is needed. The script `make_data.py` takes a soundscape, a speech dataset and a background noise dataset and returns a folder `speech` and a folder `no_speech` made of 3 seconds segments. To generate the synthetic dataset simply run:

```
poetry run python main_scripts/make_data.py --audio_path /PATH/TO/SOUNDSCAPE \ # demo_data/training_model/soundscape_data
                    --speech_data_dir /PATH/TO/HUMAN SPEECH/DIR \ # demo_data/training_model/human_voices
                    --noise_data_dir /PATH/TO/BACKGROUND NOISE/DIR \ # demo_data/natural_sounds/
                    --length_segments 3000 \
                    --proba_speech 0.5 \
                    --audio_out_dir PATH/TO/OUTPUT/DIR \
                    --metadata_file /PATH/TO/METADATA OUTPUT/DIR
                    --f_prefix no_prefix \
                    --include_noises 1 \
                    --include_soundscape 1
```

Note that you can generate a dataset with no soundscape, no background noises or no augmentation by changing `1` to `0` for the arguments `--include_noises` and `include_soundscape`.

### Training the model

After generating the dataset the model can be trained using the script `train_model.py`.

```
poetry run python main_scripts/train_model_optim.py \
  --train_path /PATH/TO/TRAIN/DIR \
  --test_path /PATH/TO/TEST/DIR \
  --save_path PATH/TO/SAVE/MODEL/model_weight.pt \
  --save_es PATH/TO/SAVE/MODEL/checkpoints.pt \
  --batch_size 64 \
  --num_workers 4 \
  --num_epoch 200 \
  --lr 0.01 \
  --tb_prefix 0.01 \
  --momentum 0.99
```

## Making predictions

### Predictions with ecoVAD

Using ecoVAD weights stored in the folder `model_weights` or a model you trained using the previous section, it is possible to predict the location of human speech in an audio file using `ecoVAD_predict.py`. The prediction ultimately return a json file with the `start` and `end` of the detection. 

```
poetry run python predictions/ecoVAD_predict.py \
            --input /PATH/TO/FOLDER/WITH/DATA/TO/PREDICT \ # demo_data/make_predictions
            --output /PATH/TO/OUTPUT/DIR \
            --model_path PATH/TO/MODEL/WEIGHTS \ # model_weights
            --threshold 0.8
```

Note that the input is the **folder** where all the audio files are stored.

### Predictions with pyannote

The script `pyannote_predict.py` is a wrapper around the [pyannote VAD model](https://github.com/pyannote) which generate predictions `.json` with a `start` and `end` of detection, similar to the `.json` file returned by the `ecoVAD_predict.py` file.

You can run the script using the following command:

```
poetry run python predictions/pyannote_predict.py \
            --input /PATH/TO/FOLDER/WITH/DATA/TO/PREDICT \
            --output /PATH/TO/OUTPUT/DIR
```
Note that the input is the **folder** where all the audio files are stored. Also note that one `.json` file per audio file will be generated.

### Extracting samples from the predictions

After generating one `.json` file per audio file, we can parse all data in a `.csv` file using the script `parse_json.py`. The script assume however that the name of the file begins with **YMD_HMS**. If not you may need to modify the script.

```
poetry run python utils/parse_json.py \
            --input /PATH/TO/THE/FOLDER/CONTAINING/JSON/FILES \
            --output /PATH/TO/OUTPUT/DIR/parsed_file.csv
```

We can then use `parsed_file.csv` to extract a chosen number of detection to get an idea of model performance.

```
poetry run python utils/extract_detection.py \
              --base_folder /AUDIO/DATA/DIR \
              --input_df PATH/TO/parsed_file.csv \
              --output /PATH/TO/OUTPUT/DIR \
              --nb_sample_detection 100  \
              --random_state 23
```

## Anonymize your data

The `anonymisation/anonymisation.py` script will conveniently anonymise your audio files using the `.json` files previously generated. `anonymisation.py` created a new audio file `ANONYMISED_audio.wav` in which all the detected speech sections are being silenced.

To run `anonymisation.py`:

```
poetry run python anonymisation/anonymisation.py \
                --json_folder /PATH/TO/JSON/DIR \
                --audio_folder /PATH/TO/AUDIO/DIR \
                --save_path /PATH/TO/SAVE/DIR
```