import argparse
import glob
import json
import os

from pydub import AudioSegment

def audio_anonymisation(json_file, audio_file, audio_save):
        # Initiate a list to store the parts of the audio file
        parts = []

        # Start the audio at 0 seconds
        begin = 0

        # Open the JSON containing the detections
        with open(json_file) as f:
            data = json.load(f)

        # Open the audio file // pydub might not be adequate for
        # big audio files
        audio = AudioSegment.from_file(audio_file)

        # Start the anonymization loop
        for i in range(len(data["content"])):

            # Silenced the voice part
            s = data["content"][i]["start"] * 1000

            # Get first part of soundscape
            soundscape_1 = audio[begin:s]
            parts.append(soundscape_1)

            # Compute the length of the silence (= the length of speech detection)
            e = data["content"][i]["end"] * 1000
            silence_duration = e - s

            # Add the silence to parts
            silence = AudioSegment.silent(int(silence_duration))
            parts.append(silence)

            # Re start the loop after the silence
            begin = e

        # keep the part after the last silence
        parts.append(audio[begin:])

        # Sum up all the parts
        anonym_audio = sum(parts)

        # Save the audio now anonymized
        anonym_audio.export(audio_save, format="wav")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--json_folder",
                        help='Path to the folder containing the JSON detection files',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--audio_folder",
                        help='Path to folder containing the audio files to anonymize',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--save_path",
                        help='Path folder to save the anonymized files',
                        required=True,
                        type=str,
                        )

    cli_args = parser.parse_args()

    JSON_files = glob.glob(cli_args.json_folder + "/*.json")
    audio_files = glob.glob(cli_args.audio_folder + "/*")

    for json_file, audio_file in zip(JSON_files, audio_files):

        audio_name = audio_file.split("/")[-1]
        save_name = os.sep.join([cli_args.save_path, "ANONYMISED_" + audio_name])
        audio_anonymisation(json_file, audio_file, save_name)

# docker run --rm -it -v Data:/Data -v $pwd:/app eco-acoustic:latest poetry run python anonymisation/anonymisation.py --json_folder /Data/json_eco-acoustic --audio_folder /Data/wav_files --save_path /Data/wav_files

