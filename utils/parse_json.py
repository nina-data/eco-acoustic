#!/usr/bin/env python3

"""
Script that parse multiple .json files into a dataframe.
Each row of the dataset corresponds to 1 file and has columns for
the number of detections and all the temporal information of the file
"""

import argparse
import json
import glob
import pandas as pd

def get_list(json_file):
    """Return a list containing nb of detections & temporal information & number of detections"""

    # Get the name of the audio file
    file_name = json_file.split('/')[-1].split('.')[0] + '.' + json_file.split('.')[1]

    # Isolate the temporal info
    ymd_hms = json_file.split("/")[-1].split(".")[0]
    ymdhms = ymd_hms.replace("_", "")

    # List the start - end of each detection
    start_end = []

    with open(json_file) as f:
        data = json.load(f)

        # Loop through the detections
        for detection in range(len(data["content"])):

            # start - end
            start = data["content"][detection]["start"]
            end = data["content"][detection]["end"]
            start_end.append([file_name, ymdhms, start, end])

    # Build the dataframe
    df = pd.DataFrame(start_end, columns = ['json_file', 'date', 'start', 'end'])

    return df

def get_df(list_files, output):
    """
    Return a dataframe containing nb of detections & temporal information & number of detections for
    a list of files
    """

    # Initiate empty DF
    small_dfs = []

    # Loop through the files
    for file in list_files:
        # Get the detections
        det = get_list(file)
        # Append to the df
        small_dfs.append(det)

    # concatenate all small df
    df = pd.concat(small_dfs)
    df['date'] = pd.to_datetime(df['date'])
    df.sort_values(by='date', inplace=True)

    # Write the output
    df.to_csv(output, index=False)


if __name__ == "__main__":

    # Argument parser
    parser = argparse.ArgumentParser()

    parser.add_argument("--input",
                        help='Path to the folder to process',
                        required=True,
                        type=str)

    parser.add_argument("--output",
                        help='Output file (has to be a csv)',
                        required=True,
                        type=str)

    cli_args = parser.parse_args()

    # Get all the files in the input folder
    list_files = glob.glob(cli_args.input + "/*.json")

    # Get the DF as a csv
    get_df(list_files, cli_args.output)


# Test script:
# docker run -it --rm -v  /home/benjamin.cretois/Data/:/Data/ -v /home/benjamin.cretois/CodeProjects/eco-acoustic/:/app/ eco-acoustic:latest poetry run python /app/utils/parse_json.py --input /Data/detections_json/citynet/audiomoth3 --output /Data/parsed_json_citynet/audiomoth3.csv
