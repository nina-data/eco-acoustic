#!/usr/bin/env python3

"""Script that takes random files from a folder and create another folder containing those files"""
import argparse
import glob
import os
import random
from shutil import copyfile

def copy_subset(input, output, proportion):

    # List the files
    list_files = glob.glob(input + "/*")

    # Get the number of files to be copied
    n = len(list_files) * proportion

    # Round to an integer (if uneven number of files)
    n = int(round(n))

    # Take a random sample
    random_files = random.sample(list_files, n)

    # copy the files
    for f in random_files:
        copyfile(f, os.path.join(output, f.split("/")[-1]))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--input",
                        help='Input folder',
                        required=True,
                        type=str)

    parser.add_argument("--output",
                        help='Output folder',
                        required=True,
                        type=str)
    parser.add_argument("--proportion",
                        help='proportion of files to be copied',
                        required=True,
                        type=float)
    cli_args = parser.parse_args()

    copy_subset(cli_args.input, cli_args.output, cli_args.proportion)

# ./utils/dataset_for_total_model.py --input $HOME/Data/wav_files --output $HOME/Data/ --proportion 0.5