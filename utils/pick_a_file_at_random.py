import os
import random

def pick_a_file_at_random(to_open_path):
    """
    Pick a random file in a random directory
    """
    
    # List all the folders inside the path
    list_dir = os.listdir(to_open_path)
    
    # Select a random folder to open
    r = random.randint(0, len(list_dir) - 1)
    to_open = list_dir[r]
    
    # Full path to to_open
    to_open_path = os.sep.join([to_open_path, to_open])
    
    if os.path.isdir(to_open_path) is True:
        to_open_path = pick_a_file_at_random(to_open_path)

    # Return the path of the file
    return to_open_path