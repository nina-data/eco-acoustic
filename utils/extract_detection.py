import argparse
import pandas as pd
import os

from pydub import AudioSegment

def get_sample_detections(base_folder, input_df, output, nb_sample_detections, random_state):

    f = pd.read_csv(input_df)
    random_sample = f.sample(n=nb_sample_detections, random_state=random_state)

    for row in range(len(random_sample)):

        # Extract the information from the row
        audio_file = random_sample.iloc[row]['json_file']
        start_detection = random_sample.iloc[row]['start'] * 1000
        end_detection = random_sample.iloc[row]['end'] * 1000

        # Get the full path of the segment
        audio_file_path = os.path.join(base_folder, audio_file)

        # Extract the detection
        audio = AudioSegment.from_file(audio_file_path)
        detection = audio[start_detection:end_detection]

        # Save the file
        audio_file_save = audio_file.split(".")[0]
        detection.export(os.path.join(output, audio_file_save + "-{}-{}.wav".format(round(start_detection, 2),
                                                                                 round(end_detection, 2))),
                         format='wav')

if __name__=='__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--base_folder",
                        help='The folder containing the WAV files to be processed',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--input_df",
                        help='Path to the DF storing the detection info',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--output",
                        help='Path to the output folder that stores the detections',
                        required=True,
                        type=str,
                        )

    parser.add_argument("--nb_sample_detection",
                        help='Number of detection samples from the model',
                        default=100,
                        type=int,
                        )

    parser.add_argument("--random_state",
                        help='Random seed',
                        default=17,
                        type=int,
                        )

    cli_args = parser.parse_args()

    get_sample_detections(cli_args.base_folder,
                          cli_args.input_df,
                          cli_args.output,
                          cli_args.nb_sample_detection,
                          cli_args.random_state)

# docker run -it --rm -v /home/benjamin.cretois/Data/:/Data/ -v $PWD:/app/ eco-acoustic:latest poetry run python /app/extract_detection.py --input /Data/json_eco-acoustic/pyannote --output /app/out.csv
