"""
Function that takes a path to the training or test set for argument 
and return a list of audio tensors and labels
"""

import os
import numpy as np

def list_audio(path):

    label_list = []
    tensor_list = []
    
    labels = os.listdir(path)

    for i in range(len(labels)):
        path_files = os.sep.join([path_segments, labels[i]])
        files = os.listdir(path_files)

        for j in range(len(files)):
            
            full_path_file = os.sep.join([path_files, files[j]])
            audio = AudioSegment.from_wav(full_path_file)
            
            tensor_list.append(audio)
            label_list.append(labels[i])

    return (tensor_list, label_list)