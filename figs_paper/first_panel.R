library(tidyverse)
library(readxl)
library(ggplot2)
library(pROC)
library(patchwork)
source("helper_functions.R")

# General options for the plots
size_text=11
size_axis_title = 11

# Set the BASE PATH
BASE_PATH = "data_for_plots/playback_results/"

#################################
###### Script for analysis ######
#################################

# Open the dataset
data <- load_xlsx(file.path(BASE_PATH, "augmented_audio_4-total-ckpt-lr-0.01.xlsx"))
data$true_label <- as.character(data$true_label)

# Make the plot
p1 <- data %>% 
  mutate(person = factor(person, levels=c("wouter", "raghnild", "kid"))) %>% 
  mutate(person = recode(person, "wouter" = "man", "raghnild" = "woman", "kid" = "child")) %>%
  ggplot(aes(x=distance, y=pred_label, 
             group= interaction(distance, person, true_label))) +
  geom_boxplot(width=0.5, 
               outlier.alpha = 0.1, 
               aes(fill=true_label, 
                   color=person, 
                   alpha=0.01), outlier.shape = NA) +
  theme_bw() + 
  xlab("Distance") + 
  ylab("Model outcome")  +
  guides(alpha="none") +
  theme(axis.text = element_text(size=size_text),
        axis.title = element_text(size = size_axis_title),
        legend.text = element_text(size=size_text),
        legend.title = element_text(size=size_axis_title)) +
  scale_fill_manual(values=c("#808080", "#FFFFFF"), labels = c("No speech", "Speech"), name = "Label") + 
  scale_color_manual(values=c("#472775ff", "#249d88ff", "#f68f46b2", "#472775ff", "#249d88ff", "#f68f46b2"), name = "Person") 
p1

# Get a summary of the data
data %>% 
  mutate(person = factor(person, levels=c("wouter", "raghnild", "kid"))) %>% 
  filter(true_label == 1) %>% 
  group_by(person, distance) %>% 
  mutate(person = recode(person, "wouter" = "man", "raghnild" = "woman", "kid" = "child")) %>% 
  summarise(mean = mean(pred_label))

###################################################
# COMPARISON AUGMENTED / NO NOISE / NO SOUNDSCAPE #
###################################################

# Open the results on the different augmentation types
# NOTE THAT WE SELECT THE LEARNING RATE GIVING THE BEST AUC !

no_noise <- total_data(file.path(BASE_PATH, "augmented_audio_4_no_noises-total-ckpt-lr-0.01.xlsx"),
                       file.path(BASE_PATH, "augmented_audio_4_no_noises-forest-ckpt-lr-0.001.xlsx"),
                       file.path(BASE_PATH, "augmented_audio_4_no_noises-open_landscape-ckpt-lr-0.001.xlsx"),
                       0.2) %>%
  mutate(augmentation = "no_noise")

no_soundscape <- total_data(file.path(BASE_PATH, "augmented_audio_4_no_soundscape-total-ckpt-lr-0.01.xlsx"),
                            file.path(BASE_PATH, "augmented_audio_4_no_soundscape-forest-ckpt-lr-0.001.xlsx"),
                            file.path(BASE_PATH, "augmented_audio_4_no_soundscape-open_landscape-ckpt-lr-0.001.xlsx"),
                            0.2)  %>% 
  mutate(augmentation = "no_soundscape")

fully_augmented <- total_data(file.path(BASE_PATH, "augmented_audio_4-total-ckpt-lr-0.01.xlsx"),
                              file.path(BASE_PATH, "augmented_audio_4-forest-ckpt-lr-0.001.xlsx"),
                              file.path(BASE_PATH, "augmented_audio_4-open_landscape-ckpt-lr-0.001.xlsx"),
                              0.2)  %>% 
  mutate(augmentation = "augmented")

no_augmentation <- total_data(file.path(BASE_PATH, "augmented_audio_4_no_soundscape_no_noises-total-ckpt-lr-0.001.xlsx"),
                              file.path(BASE_PATH, "augmented_audio_4_no_soundscape_no_noises-forest-ckpt-lr-0.001.xlsx"),
                              file.path(BASE_PATH, "augmented_audio_4_no_soundscape_no_noises-open_landscape-ckpt-lr-0.001.xlsx"),
                              0.2)  %>% 
  mutate(augmentation = "no_augmentation")

# Bind all rows to get a full dataset
data_augmentation <- rbind(no_noise, no_soundscape, fully_augmented, no_augmentation)

# Make an AUC column
data_augmentation_auc <- data_augmentation %>% 
  group_by(augmentation, model) %>% 
  mutate(true_label_factor = as.factor(true_label)) %>% 
  summarise(auc = auc(response = true_label_factor, predictor = pred_label)[1])

# Recode the names for the plot
data_augmentation_plot <- data_augmentation_auc %>% 
  mutate(augmentation = recode(augmentation, "augmented" = "ecoVAD", 
                               "no_augmentation" = "No aug",
                               "no_noise" = "Soundscape aug",
                               "no_soundscape" = "Noise aug")) %>% 
  mutate(model = recode(model, "forest" = "Forest", 
                        "ol" = "Open landscape",
                        "total" = "Forest + Open landscape"))

# Do some manipulation for the histogram order
lev_type <- c("Forest + Open landscape", "Forest", "Open landscape")
lev_aug <- c("ecoVAD", "Noise aug", "Soundscape aug", "No aug")
data_augmentation_plot$augmentation <- factor(data_augmentation_plot$augmentation, levels = lev_aug)
data_augmentation_plot$model <- factor(data_augmentation_plot$model, levels = lev_type)

# Finally plot
p2 <- data_augmentation_plot %>% 
  ggplot(aes(x = augmentation, y = auc, color = model, fill= model)) + 
  geom_bar(stat="identity", position="dodge", width = 0.8, alpha=0.5) +
  scale_fill_manual(values=c("#472775ff", "#249d88ff", "#f68f46b2"), name="Model") + 
  scale_color_manual(values=c("#472775ff", "#249d88ff", "#f68f46b2")) +
  theme_bw() +
  theme(axis.text = element_text(size=size_text),
        axis.title = element_text(size = size_axis_title),
        legend.text = element_text(size=size_text),
        legend.title = element_text(size=size_axis_title),
        axis.text.x = element_text(angle = 90)) +
  guides(text="none", color="none") +
  xlab(" ") +
  coord_cartesian(ylim = c(0.5, 1)) 
p2


###############################
### PLOT MODELS VS DISTANCE ###
###############################

# Open the predictions made by the different models
vgg11 = load_xlsx(file.path(BASE_PATH, "augmented_audio_4-total-ckpt-lr-0.01.xlsx")) %>% 
  mutate(label_output = ifelse(pred_label > 0.6, 1, 0)) %>% 
  mutate(confusion_matrix = case_when(
    true_label==1 & label_output==1 ~ "tp",
    true_label==0 & label_output==1 ~ "fp",
    true_label==1 & label_output==0 ~ "fn",
    true_label==0 & label_output==0 ~ "tn"
  ))
pyannote = read_xlsx(file.path(BASE_PATH, "pyannote_50.xlsx"))
webrtcvad = read_xlsx(file.path(BASE_PATH, "webrtcvad_50.xlsx"))

# Manipulate dataset to combine them
meta <- vgg11 %>% select(file = file_name, landscape_type, distance, position, person)
vgg11 <- vgg11 %>% 
  select(file = file_name, pred_label, true_label, confusion_matrix) %>% 
  mutate(model = "vgg11") %>% 
  full_join(., meta, by="file")

pyannote <- pyannote %>% 
  mutate(file = str_split_fixed(pyannote$file_name, "/", 10)[,10]) %>% 
  select(pred_label, true_label, file, confusion_matrix) %>% 
  mutate(model = "pyannote") %>% 
  full_join(., meta, by="file")

# For webrtcvad, create a confusion matrix column
tp <- webrtcvad$pred_label == 1 & webrtcvad$true_label == 1
tn <- webrtcvad$pred_label == 0 & webrtcvad$true_label == 0
fp <- webrtcvad$pred_label == 1 & webrtcvad$true_label == 0
fn <- webrtcvad$pred_label == 0 & webrtcvad$true_label == 1

confusion_matrix = vector()

for(i in 1:length(tp)){
  if(tp[i] == TRUE){confusion_matrix[i] = "tp"}
  else if(tn[i] == TRUE){confusion_matrix[i] = "tn"}
  else if(fp[i] == TRUE){confusion_matrix[i] = "fp"}
  else{confusion_matrix[i] = "fn"}
}

webrtcvad$confusion_matrix = confusion_matrix
webrtcvad <- webrtcvad %>%  
  select(pred_label, true_label, file = file_name, confusion_matrix) %>%
  mutate(model = "webrtcvad") %>% 
  full_join(., meta, by="file")

# Get a full dataset
combined <- rbind(vgg11, pyannote, webrtcvad)

# Precision / accurracy with distance
pr_d = combined %>% 
  group_by(confusion_matrix, model, distance) %>% 
  summarise(n = n()) %>% 
  pivot_wider(names_from = confusion_matrix, values_from = n) %>% 
  mutate(precision = tp / (tp + fp)) %>% 
  mutate(recall = tp / (tp + fn)) %>% 
  select(model, distance, precision, recall)
pr_d$distance = factor(pr_d$distance, levels = c("1m", "5m", "10m", "20m"))

# Compute the F1 score for all
pr_d$f1 <-  2 * ((pr_d$precision * pr_d$recall) / (pr_d$precision + pr_d$recall))
pr_d %>% group_by(model) %>% summarise(av = mean(f1))

# Finally plot
p3 <- pr_d %>% 
  mutate(distance_car = as.character(distance)) %>% 
  separate(distance_car, "distance_m", "m") %>% 
  mutate(distance_m = as.numeric(distance_m)) %>% 
  mutate(model = recode(model, "vgg11" = "ecoVAD")) %>% 
  ggplot(aes(x=distance_m, y = f1, col=model)) +
  geom_point(size=3, alpha=.3) +
  geom_line(aes(group=model), size=1.5, alpha=.5) +
  scale_color_manual(values=c("#472775ff", "#249d88ff", "#f68f46b2"), name = "Model") +
  theme_bw() +
  theme(axis.text = element_text(size=size_text),
        axis.title = element_text(size = size_axis_title),
        legend.text = element_text(size=size_text),
        legend.title = element_text(size=size_axis_title),
        axis.title.x = element_text(vjust = 20)) +
  ylab("F1 score") +
  xlab("Distance (in meters)") +
  scale_x_log10(breaks = c(1,5,10,20)) 
ggsave("plots/f1score_vs_distance.png")

################
# TOTAL PANEL #
###############
patch1 <- p1 / (p2 + p3)
patch_tot1 <- patch1 + plot_annotation(tag_levels = c('A', '1'),
                         tag_suffix = ')')
ggsave("plots/panel1.png", width = 10, height = 7, dpi = 300)

