# Helper to open the datasets for the playback analysis
load_xlsx <- function(xlsx){
  data = read_xlsx(xlsx)
  data$person[data$person == "ragnild" | data$person == "ragnhild"] <- "raghnild"
  data$distance = factor(data$distance, levels = c("1m", "5m", "10m", "20m"))
  return(data)
}

# Helper to build a dataset gathering all the playback analysis
total_data <- function(xlsx1, xlsx2, xlsx3, threshold){
  total <- load_xlsx(xlsx1) %>% 
    mutate(model="total")
  forest <- load_xlsx(xlsx2) %>% 
    mutate(model="forest")
  ol <- load_xlsx(xlsx3) %>% 
    mutate(model="ol")
  data_tot <- rbind(total, forest, ol)
  
  data_tot <- rbind(total, forest, ol) %>% 
    mutate(label_output = ifelse(pred_label > threshold, 1, 0)) %>% 
    mutate(confusion_matrix = case_when(
      true_label==1 & label_output==1 ~ "tp",
      true_label==0 & label_output==1 ~ "fp",
      true_label==1 & label_output==0 ~ "fn",
      true_label==0 & label_output==0 ~ "tn"
    )
    )
  return(data_tot)
}

# Function to compute f1 score
f1_score <- function(precision, recall){ 2 * ((precision * recall) / (precision + recall))}



