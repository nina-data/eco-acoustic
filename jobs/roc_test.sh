#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=12:0:0

# Load modules for any applications

module load anaconda3/personal
module load ffmpeg
module load parallel

# Set the variables
PATH_FOLDER=/rds/general/project/box-migration-ss7412/live/playback_data/
PATH_MODELS=/rds/general/user/ss7412/home/eco-acoustic/sarab_models/
PATH_FIGURES=/rds/general/user/ss7412/home/eco-acoustic/figs/test_figs/

cd $PATH_MODELS
models=( *.pt )
cd -

# Run predictions for all models
parallel -j4 ~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/roc_test.py \
      --input $PATH_FOLDER \
      --model_weight $PATH_MODELS/{} \
      --output $PATH_FIGURES \
      --prefix_figure $(basename {}.pt) \
      ::: {models[@]}