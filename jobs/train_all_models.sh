#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=12:0:0
#PBS -J 1-4

# Load modules for any applications
module load anaconda3/personal
module load ffmpeg

# Change to the submission directory
cd ~/eco-acoustic
BASE=$EPHEMERAL
OUT_DIR=$HOME/eco-acoustic/bymarka_models_fast

# Set global counter for array jobs
#let i=0

# Run the models through all the folders
for audio_base_dir in $BASE/augmented_audio \
                      $BASE/augmented_audio_no_noise \
                      $BASE/augmented_audio_no_soundscape \
                      $BASE/augmented_audio_no_soundscape_no_noise
do

  data_path=$audio_base_dir/
  prefix="$(basename $audio_base_dir)"-FAST-

  let i++
  if [ $i -eq $PBS_ARRAY_INDEX ]
  then

    for LR in 0.01
    do
      ~/anaconda3/envs/ben_env/bin/python train_model_v2.py \
	  	--data_path $data_path \
	  	--save_path $OUT_DIR/$prefix-model_weight-lr-$LR.pt \
	  	--save_es $OUT_DIR/$prefix-ckpt-lr-$LR.pt \
	  	--batch_size 64 \
	  	--num_workers 4 \
	  	--num_epoch 200 \
	  	--lr $LR \
	  	--tb_prefix $prefix-lr-$LR \
	  	--momentum 0.99   

    done
  fi
done


