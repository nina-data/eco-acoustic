#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=48:0:0
#PBS -J 1-2

# Load modules for any applications
module load anaconda3/personal-next
module load ffmpeg

# Change working directory
cd ~/eco-acoustic/

# Set the global variables
BASE_FOLDER="/rds/general/project/box-migration-ss7412/live/bymarka_audio"
OUT_FOLDER="/rds/general/user/ss7412/home/eco-acoustic/figs/detections_json/webrtcvad"

let i=0

for audiomoth in audiomoth2 audiomoth3
do

    mkdir -p $OUT_FOLDER/$audiomoth

    let i++
    if [ $i -eq $PBS_ARRAY_INDEX ]
    then
        ~/anaconda3/envs/ben_env/bin/python webrtc_predict.py \
            --input $BASE_FOLDER/$audiomoth \
            --output $OUT_FOLDER/$audiomoth/ \
            --frame_length 30 \
            --agressiveness 1
    fi
done