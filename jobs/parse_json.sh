#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=2:0:0
#PBS -J 1-2

# Load modules for any applications

module load anaconda3/personal-next

let i=0

# Run program, passing the index of this subjob within the array
BASE_DETECTIONS=/rds/general/user/ss7412/home/eco-acoustic/figs/detections_json/
OUT_FOLDER=/rds/general/user/ss7412/home/eco-acoustic/figs/parsed_json/

for folder in webrtcvad
do
	let i++
	if [ $i -eq $PBS_ARRAY_INDEX ]
	then

	mkdir -p $OUT_FOLDER/$folder

	for audiomoth in audiomoth2 audiomoth3
	do
	  ~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/parse_json.py \
	    --input $BASE_DETECTIONS/$folder/$audiomoth/ \
	    --output $OUT_FOLDER/$folder/$audiomoth.csv
	done
	fi
done
