#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=24:0:0

# Load modules for any applications

module load anaconda3/personal
module load ffmpeg
module load parallel

# Set the variables
PATH_FOLDER=/rds/general/user/ss7412/ephemeral/
PATH_MODELS=/rds/general/user/ss7412/home/eco-acoustic/ben_models/
PATH_FIGURES=/rds/general/user/ss7412/home/eco-acoustic/figs/val_figs/

cd $PATH_MODELS
models=( *.pt )
cd -

for audio_base_dir in $PATH_FOLDER/augmented_audio/test/ \
                      $PATH_FOLDER/augmented_audio_no_noise/test/ \
                      $PATH_FOLDER/augmented_audio_no_soundscape/test/ \
                      $PATH_FOLDER/augmented_audio_no_soundscape_no_noise/test/
do
  # Run predictions for all models, parallelize through the GPU
  for model in ${models[@]}
  do
    for landscape in total
    do
    ~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/roc_val.py \
          --input $audio_base_dir/$landscape \
          --model_weight $PATH_MODELS/$model \
          --output $PATH_FIGURES \
          --prefix_figure $landscape-$model #$(basename {}.pt)
          #::: ${models[@]}
    done
  done
done
