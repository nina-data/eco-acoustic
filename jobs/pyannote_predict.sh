#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=12:0:0

# Load modules for any applications
module load anaconda3/personal-next
module load ffmpeg

BASE_FOLDER="/rds/general/project/box-migration-ss7412/live/bymarka_audio"
OUT_FOLDER="/rds/general/user/ss7412/home/eco-acoustic/figs/detection_json/pyannote"

~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/pyannote_predict.py \
 --input $BASE_FOLDER/audiomoth3/ \
  --output $OUT_FOLDER/audiomoth3/