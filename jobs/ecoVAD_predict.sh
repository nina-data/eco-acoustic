#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=12:0:0
#PBS -J 1-4

# Load modules for any applications
module load anaconda3/personal-next
module load ffmpeg

BASE_FOLDER="/rds/general/project/box-migration-ss7412/live/bymarka_audio"
OUT_FOLDER="/rds/general/user/ss7412/home/eco-acoustic/figs/detections_json/"
MODELS_FOLDER="/rds/general/user/ss7412/home/eco-acoustic/bymarka_models_fast/"

let i=0

for BASE in augmented_audio augmented_audio_no_noise augmented_audio_no_soundscape augmented_audio_no_soundscape_no_noise 
do

                let i++
                if [ $i -eq $PBS_ARRAY_INDEX ]
                then

			mkdir -p $OUT_FOLDER/$BASE-thr08-lr-0.01/audiomoth{2,3}

			for audiomoth in audiomoth2 audiomoth3
			do
				~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/ecoVAD_predict.py \
			 	--input $BASE_FOLDER/$audiomoth/ \
			  	--output $OUT_FOLDER/$BASE-thr08-lr-0.01/$audiomoth/ \
			  	--model_path $MODELS_FOLDER/$BASE-FAST--ckpt-lr-0.01.pt \
			  	--threshold 0.8
			done
		fi
done
