#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=4:0:0
#PBS -J 1-4

# Load modules for any applications

module load anaconda3/personal
module load ffmpeg

# Set the variables
PATH_FOLDER=/rds/general/user/ss7412/ephemeral/
PATH_MODELS=/rds/general/user/ss7412/home/eco-acoustic/bymarka_models/
PATH_FIGURES=/rds/general/user/ss7412/home/eco-acoustic/figs/val_figs_bymarka/

cd $PATH_MODELS
models=( *.pt )
cd -

let i=0

for audio_base_dir in $PATH_FOLDER/augmented_audio_bymarka_AGGRESSIVE/test/ \
                      $PATH_FOLDER/augmented_audio_bymarka_AGGRESSIVE_no_noise/test/ \
                      $PATH_FOLDER/augmented_audio_bymarka_AGGRESSIVE_no_soundscape/test/ \
                      $PATH_FOLDER/augmented_audio_bymarka_AGGRESSIVE_no_soundscape_no_noise/test/
do
	let i++
	if [ $i -eq $PBS_ARRAY_INDEX ]
	then

		FIG_FOLDER=$(cd $audio_base_dir/.. && basename $PWD)
		mkdir -p $PATH_FIGURES/$FIG_FOLDER

		for model in ${models[@]}
		do
			~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/roc_val.py \
								  --input $audio_base_dir/ \
								  --model_weight $PATH_MODELS/$model \
								  --output $PATH_FIGURES/$FIG_FOLDER \
								  --prefix_figure $model
		done
	fi
done
