#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=24:0:0
#PBS -J 1-100

# Load modules for any applications
module load anaconda3/personal
module load ffmpeg

############################
# Set the global variables #
############################
eco_audio_dir=/rds/general/user/ss7412/home/ben_audio_datasets/Ecosystem_sound
speech_data_dir=/rds/general/project/box-migration-ss7412/live/ben_audio_datasets/LibriSpeech/train-clean-360
noise_data_dir=/rds/general/project/box-migration-ss7412/live/ben_audio_datasets/Noise_background

#set -euo pipefail

# Change to the submission directory
cd ~/eco-acoustic

########################################
# Run make_data.py for all possibility #
########################################

# Set global counter for array jobs
let i=0

for include_soundscape in 0 1
do
  for include_noises in 0 1
  do
    # Make a directory to store the segments
    out_base_dir=/rds/general/user/ss7412/ephemeral/augmented_audio_AGGRESSIVE2
    metadata_base_out_dir=/rds/general/user/ss7412/ephemeral/augmented_metadata_AGGRESSIVE2

    # Create the data folders for all possibilities -> augmented_audio / augmented_audio_no_noise / augmented_audio_no_soundscape &
    # augmented_audio_no_noise_no_soundscape
    if [ "$include_soundscape" -eq 0 ]
    then
      suffix=_no_soundscape
      out_base_dir=$out_base_dir$suffix
      metadata_base_out_dir=$metadata_base_out_dir$suffix
    fi

    if [ "$include_noises" -eq 0 ]
    then
      suffix=_no_noise
      out_base_dir=$out_base_dir$suffix
      metadata_base_out_dir=$metadata_base_out_dir$suffix
    fi

    # Create the metadata & segment storage directories
    mkdir -p $out_base_dir
    mkdir -p $metadata_base_out_dir

    # Loop through the landscape type
    for landscape in 'forest' 'open_landscape'
    do
      # Set a random seed
      RANDOM=17

      # Loop through the runtype
      for runtype in 'train' 'test'
      do

      # List all the concat files
      concat_files=$eco_audio_dir/$runtype/$landscape/concat/*.wav
      mkdir -p $out_base_dir/$runtype/$landscape/
      mkdir -p $metadata_base_out_dir/$runtype/$landscape/

        # For each .wav file run the script
        for f in $concat_files
        do
          let i++
          if [ $i -eq $PBS_ARRAY_INDEX ]
          then
          ~/anaconda3/envs/ben_env/bin/python make_data_total.py --audio_path $f \
                                                            --speech_data_dir $speech_data_dir \
                                                            --noise_data_dir $noise_data_dir \
                                                            --length_segments 3000 \
                                                            --proba_speech 0.5 \
                                                            --audio_out_dir $out_base_dir/$runtype/$landscape/ \
                                                            --metadata_file $metadata_base_out_dir/$runtype/$landscape/$(basename $f.xlsx) \
                                                            --f_prefix $(basename $f) \
                                                            --include_noises "$include_noises" \
                                                            --include_soundscape "$include_soundscape"
          fi
        done
      done
    done
  done
done
