#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=24:0:0
#PBS -J 1-4

# Load modules for any applications
module load anaconda3/personal
module load ffmpeg

# Change to the submission directory
cd ~/eco-acoustic
BASE=$EPHEMERAL
OUT_DIR=$HOME/eco-acoustic/bymarka_models

# Set global counter for array jobs
let i=0

# Run the models through all the folders
for audio_base_dir in $BASE/augmented_audio \
                      $BASE/augmented_audio_no_noise \
                      $BASE/augmented_audio_no_soundscape \
                      $BASE/augmented_audio_no_soundscape_no_noise
do
    prefix="$(basename $audio_base_dir)"

    let i++
    if [ $i -eq $PBS_ARRAY_INDEX ]
    then
    
        for LR in 0.001
        do
        ~/anaconda3/envs/ben_env/bin/python -u train_model_simple.py \
                    --data_path $audio_base_dir \
                    --save_path $OUT_DIR/$prefix-weights-lr-$LR.pt \
                    --save_es $OUT_DIR/$prefix-ckpts-lr-$LR.pt \
                    --batch_size 128 \
                    --lr $LR \
                    --num_epoch 100 \
                    --tb_prefix $prefix-lr-$LR
        done
    fi
done
