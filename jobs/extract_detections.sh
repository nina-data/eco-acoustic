#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=2:0:0
#PBS -J 1-2

# Load modules for any applications

module load anaconda3/personal
module load ffmpeg

# Change to the submission directory

cd ~/eco-acoustic

let i=0

# Run program, passing the index of this subjob within the array
BASE_FOLDER=/rds/general/project/box-migration-ss7412/live/bymarka_audio

for folder in figs/parsed_json/webrtcvad 
do
	let i++
	if [ $i -eq $PBS_ARRAY_INDEX ]
	then

	OUTPUT_FOLDER=figs/sample_detections/$(basename $folder)
	mkdir -p $OUTPUT_FOLDER/audiomoth{2,3}

	for audiomoth in audiomoth2 audiomoth3
	do

	  ~/anaconda3/envs/ben_env/bin/python extract_detection.py \
	      --base_folder $BASE_FOLDER/$audiomoth/ \
	      --input_df $folder/$audiomoth.csv \
	      --output $OUTPUT_FOLDER/$audiomoth \
	      --nb_sample_detection 100  \
	      --random_state 42
	done
	fi
done
