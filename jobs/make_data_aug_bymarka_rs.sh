#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=72:0:0
#PBS -J 1-12

# Load modules for any applications
module load anaconda3/personal
module load ffmpeg

# Change to the submission directory
cd ~/eco-acoustic

~/anaconda3/envs/ben_env/bin/python make_data_random_seed.py
