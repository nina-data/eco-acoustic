#PBS -lselect=1:ncpus=4:mem=32gb
#PBS -lwalltime=24:0:0
#PBS -J 1-8

let i=0

for audio_base_dir in $BASE/augmented_audio_random \
                      $BASE/augmented_audio_random_no_noise \
                      $BASE/augmented_audio_random_no_soundscape \
                      $BASE/augmented_audio_random_no_soundscape_no_noise
do
    for is_speech in speech no_speech
    do
        let i++
        if [ $i -eq $PBS_ARRAY_INDEX ]
        then
            rm $audio_base_dir/$is_speech/2021100{2,3}*
        fi    
    done
done
