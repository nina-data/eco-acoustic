#PBS -lselect=1:ncpus=4:mem=24gb:ngpus=1:gpu_type=RTX6000
#PBS -lwalltime=4:0:0
#PBS -J 1-3

# Load modules for any applications
module load anaconda3/personal
module load ffmpeg

# Run program
BASE_DIR=/rds/general/user/ss7412/ephemeral/augmented_audio_bymarka

# Set the train / test path
m_train_path=$BASE_DIR/train/
m_test_path=$BASE_DIR/test/

# Parallelize the workflow
LR=( 0.1 0.01 0.001 )
lr=${LR[$PBS_ARRAY_INDEX-1]}

prefix="$(basename $BASE_DIR)"

~/anaconda3/envs/ben_env/bin/python ~/eco-acoustic/train_model_optim.py \
  --train_path $m_train_path \
  --test_path $m_test_path \
  --save_path ~/eco-acoustic/ben_models/$prefix-model_weight-lr-$lr.pt \
  --save_es ~/eco-acoustic/ben_models/$prefix-ckpt-lr-$lr.pt \
  --batch_size 64 \
  --num_workers 4 \
  --num_epoch 200 \
  --lr $lr \
  --tb_prefix $prefix-lr-$lr \
  --momentum 0.99